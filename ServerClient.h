#pragma once
class ServerClient
{
public:
	char hostname[15] = "192.168.1.119";
	bool connected = false;
	pthread_t recv_thread;
	pthread_t ping_thread;
	int remote_socket;

	ServerClient();
	~ServerClient();

	bool connect_server();
	void onRecvd(void(*callback)(char*, ssize_t size));
	void send_packet(char* buf, size_t buf_size);
	void send_byte(char _byte);
private:
	static void* recv_thread_start(void* arg);
	static void * ping_thread_start(void * arg);
	void(*recv_callback)(char*, ssize_t size);
};

