﻿using System;

namespace SERVER {
	class Program {
		static void Main(string[] args) {
			IoTServer server = new IoTServer();
			server.Start();

			bool test = false;

			while(true) {
				Console.ReadKey();
				foreach(Client c in server.ClientList) {
					if(c.GetType() == typeof(SmartSwitch)) {
						if(test) {
							c.SendPacket(0xB0);
							test = false;
						}
						else {
							c.SendPacket(0xB1);
							test = true;
						}
					}
				}
			}

			server.JoinListenThread();
		}
	}
}
