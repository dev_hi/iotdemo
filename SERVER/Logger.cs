﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SERVER {
	public static class Logger {
		public static void Log(object caller, string message) {
			StackFrame[] frames = ((new StackTrace()).GetFrames());
			string m = "[" + DateTime.Now.ToString("HH:mm:ss") + "] " + caller.GetType().Name + "::" + frames[1].GetMethod().Name + "() >> " + message;
			Console.WriteLine(m);
			try {
				File.AppendAllLines("./stdout.log", new string[] { m });
			}
			catch (Exception) {

			}
		}
		public static void Log(Type caller, string message) {
			StackFrame[] frames = ((new StackTrace()).GetFrames());
			string m = "[" + DateTime.Now.ToString("HH:mm:ss") + "] " + caller.Name + "::" + frames[1].GetMethod().Name + "() >> " + message;
			Console.WriteLine(m);
			try {
				File.AppendAllLines("./stdout.log", new string[] { m });
			}
			catch (Exception) {

			}
		}
		public static void Log(object caller, Exception e) {
			StackFrame[] frames = ((new StackTrace()).GetFrames());
			string m = "[" + DateTime.Now.ToString("HH:mm:ss") + "] " + caller.GetType().Name + "::" + frames[1].GetMethod().Name + "() 예외발생 >> " + e.Message; // + e.ToString() + " - " + e.Message.Replace(e.StackTrace, "");
			string s = "#=========#  콜스택  #=========#\n" + e.StackTrace + "\n#=========# 콜스택 끝 #=========#";
			Console.Error.WriteLine(m);
#if DEBUG
			Console.Error.WriteLine(s);
#endif
			try {
				File.AppendAllLines("./stderr.log", new string[] { m, s });
			}
			catch (Exception) {

			}
		}
		public static void Log(Type caller, Exception e) {
			StackFrame[] frames = ((new StackTrace()).GetFrames());
			string m = "[" + DateTime.Now.ToString("HH:mm:ss") + "] " + caller.Name + "::" + frames[1].GetMethod().Name + "() 예외발생 >> " + e.Message;// + e.ToString() + " - " + e.Message.Replace(e.StackTrace, "");
			string s = "#=========#  콜스택  #=========#\n" + e.StackTrace + "\n#=========# 콜스택 끝 #=========#";
			Console.Error.WriteLine(m);
#if DEBUG
			Console.Error.WriteLine(s);
#endif
			try {
				File.AppendAllLines("./stderr.log", new string[] { m, s });
			}
			catch (Exception) {

			}
		}
	}
}
