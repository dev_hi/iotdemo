﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SERVER {
	public class AdminClient : Client {
		public AdminClient(Socket accepted) : base(accepted) {

		}

		protected override void HandlePacket(byte packet) {
			Logger.Log(this, String.Format("Received 0x{0:X} Packet", packet));
			if(packet == 0xC0) {
				foreach (Client c in this.ParentServer.ClientList) {
					if(c.GetType() == typeof(SmartSwitch)) {
						this.SendPacket(((IoTDevice)c).ID);
						break;
					}
				}
			}
			else if(packet == 0xB0) {
				foreach (Client c in this.ParentServer.ClientList) {
					if (c.GetType() == typeof(SmartSwitch)) {
						c.SendPacket(0xB0);
						break;
					}
				}
			}
			else if(packet == 0xB1) {
				foreach (Client c in this.ParentServer.ClientList) {
					if (c.GetType() == typeof(SmartSwitch)) {
						c.SendPacket(0xB1);
						break;
					}
				}
			}
		}
	}
}
