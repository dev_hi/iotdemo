﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SERVER {
	public class IoTServer {
		public const int DEFAULT_PORT = 27015;

		private Socket ListenSocket;
		private Thread ListenThread;
		private int BindPort = DEFAULT_PORT;
		private bool Running = false;

		public List<Client> ClientList = new List<Client>();

		public IoTServer() : this(DEFAULT_PORT) { }

		public IoTServer(int port) {
			this.ListenThread = new Thread(new ThreadStart(ListenThreadWork));
			this.ListenSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			
		}

		internal void AddClient(Client client) {
			client.ParentServer = this;
			this.ClientList.Add(client);
		}

		public void Start() {
			if(!this.Running) {
				Logger.Log(this, "server starting...");
				this.ListenSocket.Bind(new IPEndPoint(IPAddress.Any, this.BindPort));
				this.ListenSocket.Listen(100);
				this.Running = true;
				this.ListenThread.Start();
				Logger.Log(this, "Started Server, port=" + this.BindPort);
			}
		}

		public void JoinListenThread() {
			this.ListenThread.Join();
		}

		public void Stop() {
			if(this.Running) {
				this.Running = false;
				foreach (Client client in this.ClientList) {
					client.Drop();
				}
				this.ListenThread.Interrupt();
				this.ListenThread.Abort();
				this.ListenSocket.Close();
			}
		}

		public Client GetDeviceByID(byte id) {
			Client ret = null;
			foreach(Client client in this.ClientList) {
				if(client.GetType() == typeof(IoTDevice)) {
					if(((IoTDevice)client).ID == id) {
						ret = client;
						break;
					}
				}
			}
			return ret;
		}

		public void RemoveClient(Client client) {
			this.ClientList.Remove(client);
		}

		private void ListenThreadWork() {
			try {
				while (Running) {
					this.AddClient(Client.CreateClient(this.ListenSocket.Accept()));
				}
			}
			catch (ThreadInterruptedException) { }
			catch (ThreadAbortException) { }
		}
	}
}
