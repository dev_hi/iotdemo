﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SERVER {
	public abstract class Client {

		public Socket RemoteSocket;

		public Thread RecvThread;
		private Timer PingTimer;
		private bool _connected = true;
		public bool Connected { get { return this.RemoteSocket.Connected && this._connected; } }

		public delegate void IotEventHandler(Client sender, IoTEventArgs e);
		public IotEventHandler onIotEvent;

		public IoTServer ParentServer;

		private bool PingCheck = false;

		protected Client(Socket accepted) {
			this.RemoteSocket = accepted;
			this.RecvThread = new Thread(new ThreadStart(RecvThreadWork));
			this.PingTimer = new Timer(new TimerCallback(PingThreadWork), null, 0, 500000);
			this.RecvThread.Start();
		}

		private void PingThreadWork(object arg) {
			if(PingCheck) {
				this.Drop();
			}
			else {
				this.SendPacket(0);
				PingCheck = true;
			}
		}

		private void RecvThreadWork() {
			try {
				while(this.Connected) {
					byte[] buf = new byte[1];
					int recv = this.RemoteSocket.Receive(buf);
					if(recv > 0) {
						if(buf[0] == 0x00) {
							this.SendPacket(0x01);
						}
						else if(buf[0] == 0x01) {
							this.PingCheck = false;
						}
						this.HandlePacket(buf[0]);
					}
					else {
						this._connected = false;
						this.Drop();
					}
				}
			}
			catch(ThreadAbortException) { }
			catch(ThreadInterruptedException) { }
		}

		protected abstract void HandlePacket(byte packet);

		public int SendPacket(byte code) {
			if(this.RemoteSocket.Connected) {
				Logger.Log(this, String.Format("send packet code {0:X}", code));
				byte[] buf = new byte[1] { code };
				return this.RemoteSocket.Send(buf);
			}
			return 0;
		}

		public void Drop() {
			if(this.RemoteSocket.Connected) {
				this.RemoteSocket.Close();
			}
			this.RecvThread.Interrupt();
			this.RecvThread.Abort();
			this.ParentServer.RemoveClient(this);
		}

		public static Client CreateClient(Socket accepted) {
			byte[] buf = new byte[1];
			int recv = accepted.Receive(buf);
			Client ret = null;
			if(recv > 0) {
				switch(buf[0]) {
					case 0xA0:
						ret = new AdminClient(accepted);
						break;
					case 0xF0:
						ret = new Doorlock(accepted);
						break;
					case 0xF1:
						ret = new SmartSwitch(accepted);
						break;
					default:
						ret = new AdminClient(accepted);
						break;
				}
			}
			Logger.Log(typeof(Client), "new client connected., type=" + ret.ToString());
			return ret;
		}
	}
}
