﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SERVER {
	public class Doorlock : IoTDevice {
		public Doorlock(Socket accepted) : base(accepted) {
		}

		protected override void HandlePacket(byte packet) {
			Logger.Log(this, String.Format("Received 0x{0:X} Packet", packet));
		}
	}
}
