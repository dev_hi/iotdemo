﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SERVER {
	public class SmartSwitch : IoTDevice {
		public SmartSwitch(Socket accepted) : base(accepted) {
		}

		protected override void HandlePacket(byte packet) {
			Logger.Log(this, String.Format("Received 0x{0:X} Packet", packet));
			if(packet == 0x00) {
				this.SendPacket(0x01);
			}
			else if(packet == 0x01) {

			}
		}
	}
}
