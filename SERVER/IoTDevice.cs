﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SERVER {
	public abstract class IoTDevice : Client {
		public byte ID;
		protected IoTDevice(Socket accepted) : base(accepted) {
			byte[] buf = new byte[1];
			int recv = this.RemoteSocket.Receive(buf);
			if(recv > 0) {
				this.ID = buf[0];
			}
			else {

			}
		}
	}
}
