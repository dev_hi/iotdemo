/*
 * gpio_def.h
 * 
 * 참조 데이터시트 : BCM2835 Peripherals p.90
 * https://www.raspberrypi.org/app/uploads/2012/02/BCM2835-ARM-Peripherals.pdf
 * 
 */

#pragma once

//라즈베리파이 Zero W IO칩셋 베이스 어드레스
#define BASEADDR_BCM2835		0x20000000
//GPIO 컨트롤 레지스터 어드레스
#define BASEADDR_REG_GPIO		(BASEADDR_BCM2835 + 0x200000)
//IRQ 레지스터 어드레스
#define BASEADDR_REG_IRQ		(BASEADDR_BCM2835 + 0xB000)

//BCM2835 GPIO 레지스터 오프셋
#define OFFSET_GPFSEL0		0x00		//펑션 셀렉트 0-9
#define OFFSET_GPFSEL1		0x04		//펑션 셀렉트 10-19
#define OFFSET_GPFSEL2		0x08		//펑션 셀렉트 20-29
#define OFFSET_GPFSEL3		0x0C		//펑션 셀렉트 30-39
#define OFFSET_GPFSEL4		0x10		//펑션 셀렉트 40-49
#define OFFSET_GPFSEL5		0x14		//펑션 셀렉트 50-53
//		예약				0x18
#define OFFSET_GPSET0		0x1C		//3V3 아웃 0-31
#define OFFSET_GPSET1		0x20		//3V3 아웃 32-53
//		예약				0x24
#define OFFSET_GPCLR0		0x28		//0V 클리어 0-31
#define OFFSET_GPCLR1		0x2C		//0V 클리어 32-53
//		예약				0x30
#define OFFSET_GPLEV0		0x34		//인풋 0-31
#define OFFSET_GPLEV1		0x38		//인풋 32-53
//		예약				0x3C
#define OFFSET_GPEDS0		0x40		//이벤트 디텍트 상태 0-31
#define OFFSET_GPEDS1		0x44		//이벤트 디텍트 상태 32-53
//		예약				0x48
#define OFFSET_GPREN0		0x4C		//상승 엣지 디텍트 활성화 0-31
#define OFFSET_GPREN1		0x50		//상승 엣지 디텍트 활성화 32-53
//		예약				0x54
#define OFFSET_GPFEN0		0x58		//하강 엣지 디텍트 활성화 0-31
#define OFFSET_GPFEN1		0x5C		//하강 엣지 디텍트 활성화 32-53
//		예약				0x60
#define OFFSET_GPHEN0		0x64		//하이(3V3) 디텍트 활성화 0-31
#define OFFSET_GPHEN1		0x68		//하이(3V3) 디텍트 활성화 32-53
//		예약				0x6C
#define OFFSET_GPLEN0		0x70		//로우(0V) 디텍트 활성화 0-31
#define OFFSET_GPLEN1		0x74		//로우(0V) 디텍트 활성화 32-53
//		예약				0x78
#define OFFSET_GPAREN0		0x7C		//비동기 상승 엣지 디텍트 0-31
#define OFFSET_GPAREN1		0x80		//비동기 상승 엣지 디텍트 32-53
//		예약				0x84
#define OFFSET_GPAFEN0		0x88		//비동기 하강 엣지 디텍트 0-31
#define OFFSET_GPAFEN1		0x8C		//비동기 하강 엣지 디텍트 32-53
//		예약				0x90
#define OFFSET_GPPUD		0x94		//내부 풀업/풀다운 활성화 (전체 적용) 2비트만 플래그 사용
#define OFFSET_GPPUDCLK0	0x98		//내부 풀업/풀다운 활성화 클럭 0-31
#define OFFSET_GPPUDCLK1	0x9C		//내부 풀업/풀다운 활성화 클럭 32-53
//		예약				0xA0

#define GPIO_REG_MAXBIT		31			//레지스터 플래그 최대 길이

#define GPIO_FALT0			4			//펑션 셀렉트 플래그 (100=얼터네티브 펑션 0)
#define GPIO_FALT1			5			//펑션 셀렉트 플래그 (101=얼터네티브 펑션 1)
#define GPIO_FALT2			6			//펑션 셀렉트 플래그 (110=얼터네티브 펑션 2)
#define GPIO_FALT3			7			//펑션 셀렉트 플래그 (111=얼터네티브 펑션 3)
#define GPIO_FALT4			3			//펑션 셀렉트 플래그 (011=얼터네티브 펑션 4)
#define GPIO_FALT5			2			//펑션 셀렉트 플래그 (010=얼터네티브 펑션 5)


//메모리 매핑 사이즈
#define PAGE_SIZE (4*1024)
#define BLOCK_SIZE (4*1024)

//함수 export
//			returns				extern name						parameters										설명
extern		int					GpioInit						(void);											//GPIO 커널 메모리 매핑
extern		void				PrintMemCheck					(int base_addr, int start, int end);							//메모리 내용 출력
extern		void				SetFlagOnOffset					(int offset, int bit, bool flag);				//단일 플래그 설정
extern		void				SetFlagOnOffset					(int offset, int bit, int flags, int n);		//n개 비트 플래그 설정
extern		bool				GetFlagOnAddress				(int offset, int bit);							//플래그 값 가져오기
extern		int					GetFlagOnAddress				(int offset, int bit, int n);					//n개 비트 플래그 값 리턴
extern		void				SetGpioInputMode				(int num);										//GPFSEL에 해당 플래그 000(인풋)으로 설정
extern		void				SetGpioOutputMode				(int num);										//GPFSEL에 해당 플래그 001(아웃풋)으로 설정
extern		void				SetGpioAlterMode				(int num, int alt);								//GPFSEL에 해당 플래그를 다른 펑션으로 설정 (I2C, SPI등) 자세한 내용은 데이터시트 102페이지 표 6-31 참조
extern		bool				GetGpioValue					(int num);										//GPLEV에 해당 플래그 값 리턴
extern		void				SetGpioValue					(int num, bool val);							//GPSET에 해당 플래그 값 설정
extern		void				SetEventDetectStatus			(int num, bool val);
extern		void				SetRisingEdgeEnable				(int num, bool val);
extern		void				SetHighDetectEnable				(int num, bool val);
extern		void				SetLowDetectEnable				(int num, bool val);
extern		void				SetAsyncRisingEdgeDetectEnable	(int num, bool val);
extern		void				SetAsyncFallingEdgeDetectEnable	(int num, bool val);
extern		void				SetGpioPull						(int p);										//GPPUD 설정
extern		void				SetGpioPullClock				(int num, bool clkset);							//GPPUDCLK에 해당 플래그값 설정
extern		volatile unsigned*	GetGpioBase						();												//매핑된 레지스터 메모리 포인터 리턴
extern		int					WaitSysfs						(int p, int ms);								// /sys/class/gpio 폴링
extern		void				Export							(int p);