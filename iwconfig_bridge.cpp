#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string.h>
#include <string>
#include <array>

const char* exec(const char* cmd) {
	printf("exec commnad : %s\r\n", cmd);
	std::array<char, 128> buffer;
	std::string result;
	std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
	if (!pipe) {
		throw std::runtime_error("popen() failed!");
	}
	while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
		result += buffer.data();
	}
	return result.c_str();
}

void INIT_BT() {
	exec("sudo chmod 777 /var/run/sdp");
}

const char* ESSID_LIST() {
	return exec("sudo iwlist scan | grep -oP 'ESSID:\"[^\"]*\"'");
}

const char* SET_DISCOVERABLE(bool yes) {
	return exec(yes ? "sudo bluetoothctl discoverable yes" : "sudo bluetoothctl discoverable no");
}

const char* SET_BTPOWER(bool on) {
	if(on) INIT_BT();
	return exec(on ? "sudo bluetoothctl power on" : "sudo bluetoothctl power off");
}

const char* SET_WIFI_POWER(bool on) {
	return exec(on ? "sudo ifconfig wlan0 up" : "sudo ifconfig wlan0 down");
}

const char* CONNECT_WIFI(const char* ssid, const char* pass) {
	char cmd[256] = { 0 };
	snprintf(cmd, 256, "sudo iwconfig wlan0 essid %s key s:%s", ssid, pass);
	return exec(cmd);
}

const char* NETWORK_ENTRY() {
	return exec("sudo dhclient wlan0");
}

void RESET_WPA() {
	exec("sudo rm /etc/wpa_supplicant/wpa_supplicant-wlan0.conf");
	exec("sudo cp /etc/wpa_supplicant/wpa_supplicant.wipe /etc/wpa_supplicant/wpa_supplicant-wlan0.conf");
	exec("sudo service networking restart");
}

char* substr2(char* str, int s, int e) {
	char* _new = (char*)malloc(sizeof(char)*(e - s + 2));
	strncpy(_new, str + s, e - s + 1);
	_new[e - s + 1] = 0;
	return _new;
}

char* GET_IP_WLAN0() {
	char* ret = (char*)exec("ifconfig wlan0 | grep -oP 'inet [0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}'");
	int l = strlen(ret);
	if (l > 7) {
		ret = substr2(ret, 5, l - 1);
		return ret;
	}
	else {
		return "";
	}
}