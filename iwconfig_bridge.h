#pragma once
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>


extern const char* exec(const char* cmd);
extern const char* ESSID_LIST();
extern const char* SET_DISCOVERABLE(bool yes);
extern const char* SET_BTPOWER(bool on);
extern const char* SET_WIFI_POWER(bool on);
extern const char* CONNECT_WIFI(const char* ssid, const char* pass);
extern const char* NETWORK_ENTRY();
extern char* GET_IP_WLAN0();
extern void RESET_WPA();
extern void INIT_BT();