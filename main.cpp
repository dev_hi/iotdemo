#include <stdio.h>
#include <stdlib.h>
//#include "gpio_def.h"
#include <wiringPi.h>
#include <pthread.h>
#include "ServerClient.h"
#include "btserver.h"

#define GPIO_IR			16
#define GPIO_RELAY		18
#define GPIO_WIFI_LED	20
#define GPIO_POWER_LED	21
#define GPIO_BUTTON		23

bool ac_out = false;
bool temp_ac_out = false;
bool btserver_started = false;
ServerClient client;
pthread_t thread_button;
pthread_t thread_ir;
pthread_t thread_network;

void reset() {
	btserver_started = true;
	start_btserver();
}

void turn(bool on) {
	if (on) {
		digitalWrite(GPIO_RELAY, 1);
		ac_out = true;
		digitalWrite(GPIO_POWER_LED, 1);
	}
	else {
		digitalWrite(GPIO_RELAY, 0);
		ac_out = false;
		digitalWrite(GPIO_POWER_LED, 0);
	}
}

void togglePower() {
	if (ac_out) {
		turn(false);
	}
	else {
		turn(true);
	}
}

pthread_t ResetCheckThread = NULL;

void* CheckReset(void* arg) {
	delay(5000);
	if (digitalRead(GPIO_BUTTON) == 1) {
		ResetCheckThread = NULL;
		reset();
	}
}

void onButtonChange(void) {
	int val = digitalRead(GPIO_BUTTON);
	if (val == 0) {
		if (ResetCheckThread != NULL) {
			pthread_cancel(ResetCheckThread);
			ResetCheckThread = NULL;
			togglePower();
		}
	}
	else {
		pthread_create(&ResetCheckThread, NULL, CheckReset, NULL);
	}
}

void onIRChange(void) {
	int val = digitalRead(GPIO_IR);
	if (val == 0) {
		turn(false);
	}
	else {
		turn(true);
	}
}

void* thread_button_start(void* arg) {
	wiringPiISR(GPIO_BUTTON, INT_EDGE_BOTH, onButtonChange);
	while (true) {
		waitForInterrupt(GPIO_BUTTON, 1000);
	}
}

void* thread_ir_start(void* arg) {
	wiringPiISR(GPIO_IR, INT_EDGE_BOTH, onIRChange);
	while (true) {
		waitForInterrupt(GPIO_IR, 1000);
	}
}


void onReceived(char* buf, ssize_t size) {
	if (buf[0] == 0xB0) {
		turn(true);
	}
	else if (buf[0] == 0xB1) {
		turn(false);
	}
}

void toggle_wifi_led() {
	if (digitalRead(GPIO_WIFI_LED) == 0) {
		digitalWrite(GPIO_WIFI_LED, 1);
	}
	else {
		digitalWrite(GPIO_WIFI_LED, 0);
	}
}

void* thread_network_start(void* arg) {
	while (true) {
		if (digitalRead(2) == 0) btserver_started = false;
		else btserver_started = true;

		if (client.connected && !btserver_started) digitalWrite(GPIO_WIFI_LED, 1);
		else toggle_wifi_led();
		if (btserver_started) delay(500);
		else delay(100);
	}
}

void TryConnect() {
	if (!client.connected) {
		if (client.connect_server()) {
			client.onRecvd(onReceived);
			client.send_byte(0xF1);
			client.send_byte(0x5A);
		}
	}
}


int main(void)
{
	wiringPiSetupGpio();
	pinMode(2, OUTPUT);
	pinMode(GPIO_IR, INPUT);
	pinMode(GPIO_RELAY, OUTPUT);
	pinMode(GPIO_POWER_LED, OUTPUT);
	pinMode(GPIO_WIFI_LED, OUTPUT);
	pinMode(GPIO_BUTTON, INPUT);
	digitalWrite(GPIO_RELAY, 0);
	digitalWrite(GPIO_POWER_LED, 0);
	digitalWrite(GPIO_WIFI_LED, 0);
	digitalWrite(2, 0);
	pthread_create(&thread_button, NULL, thread_button_start, NULL);
	pthread_create(&thread_ir, NULL, thread_ir_start, NULL);
	pthread_create(&thread_network, NULL, thread_network_start, NULL);
	TryConnect();

	pthread_join(thread_button, NULL);
	return 0;
}