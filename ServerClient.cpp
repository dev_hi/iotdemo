#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include "ServerClient.h"



ServerClient::ServerClient()
{
	this->remote_socket = socket(AF_INET, SOCK_STREAM, 0);
}


ServerClient::~ServerClient()
{
}

bool ServerClient::connect_server() {
	struct sockaddr_in addr;
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(27015);
	addr.sin_addr.s_addr = inet_addr(this->hostname);
	bool ret = connect(this->remote_socket, (struct sockaddr*)&addr, sizeof(addr)) == 0;
	if (ret) {
		this->connected = true;
		pthread_create(&this->recv_thread, NULL, this->recv_thread_start, this);
		pthread_create(&this->ping_thread, NULL, this->ping_thread_start, this);
	}
	return ret;
}

void ServerClient::onRecvd(void(*callback)(char *, ssize_t size))
{
	this->recv_callback = callback;
}

void ServerClient::send_packet(char * buf, size_t buf_size)
{
	write(this->remote_socket, buf, buf_size);
}

void ServerClient::send_byte(char _byte)
{
	char buf[1] = { _byte };
	this->send_packet(buf, 1);
}

void* ServerClient::recv_thread_start(void* arg) {
	ServerClient* self = (ServerClient*)arg;
	while (self->connected) {
		char buf[1];
		ssize_t recv = read(self->remote_socket, buf, 1);
		if (recv > 0) {
			if (buf[0] == 0x00) {
				self->send_byte(0x01);
			}
			else if (buf[0] == 0x01) {

			}
			self->recv_callback(buf,recv);
		}
		else {

		}
	}
}

void* ServerClient::ping_thread_start(void* arg) {
	while (true) {
		sleep(500);
	}
}