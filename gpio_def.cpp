#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <poll.h>
#include <sys/ioctl.h>
#include <sys/inotify.h>
#include <sys/wait.h>
#include <pthread.h>
#include "gpio_def.h"

volatile unsigned* gpio_base;
volatile unsigned* irq_base;

bool GetFlagOnAddress(int offset, int bit) {
	return ((*(gpio_base + offset))&(1 << bit)) >> bit;
}

int GetFlagOnAddress(int offset, int bit, int n) {
	//return ((*(gpio_base + offset))&(1 << bit)) >> bit;
}

void SetFlagOnOffset(int offset, int bit, int flags, int n) {
	int a = 1;
	for (int i = 0; i < n; ++i) a |= 1 << i;
	int o = (*(gpio_base + offset));
	o &= ~(a << (bit));
	o |= (flags << (bit));
	*(gpio_base + offset) = o;
}

void SetFlagOnOffset(int offset, int bit, bool flag) {
	SetFlagOnOffset(offset, bit, (int)flag, 1);
}

void SetGpioInputMode(int num) {
 	int offset = (num < 10 ? OFFSET_GPFSEL0 :
		num < 20 ? OFFSET_GPFSEL1 :
		num < 30 ? OFFSET_GPFSEL2 :
		num < 40 ? OFFSET_GPFSEL3 :
		num < 50 ? OFFSET_GPFSEL4 :
 		num < 60 ? OFFSET_GPFSEL5 : OFFSET_GPFSEL0) / 4;
	Export(num);
	SetFlagOnOffset(offset, ((num - (offset * 10)) * 3), 0, 3);
}

void SetGpioOutputMode(int num) {
	int offset = (num < 10 ? OFFSET_GPFSEL0 :
		num < 20 ? OFFSET_GPFSEL1 :
		num < 30 ? OFFSET_GPFSEL2 :
		num < 40 ? OFFSET_GPFSEL3 :
		num < 50 ? OFFSET_GPFSEL4 :
		num < 60 ? OFFSET_GPFSEL5 : OFFSET_GPFSEL0) / 4;
	Export(num);
	SetFlagOnOffset(offset, ((num - (offset * 10)) * 3), 1, 3);
}

void SetGpioAlterMode(int num, int alt) {
	int offset = (num < 10 ? OFFSET_GPFSEL0 :
		num < 20 ? OFFSET_GPFSEL1 :
		num < 30 ? OFFSET_GPFSEL2 :
		num < 40 ? OFFSET_GPFSEL3 :
		num < 50 ? OFFSET_GPFSEL4 :
		num < 60 ? OFFSET_GPFSEL5 : OFFSET_GPFSEL0) / 4;
	int flag = GPIO_FALT0;
	switch (alt)
	{
	case 1:
		flag = GPIO_FALT1;
		break;
	case 2:
		flag = GPIO_FALT2;
		break;
	case 3:
		flag = GPIO_FALT3;
		break;
	case 4:
		flag = GPIO_FALT4;
		break;
	case 5:
		flag = GPIO_FALT5;
		break;
	}
	printf("flag = %d\r\n", flag);
	SetFlagOnOffset(offset, ((num - (offset * 10)) * 3), flag, 3);
}

bool GetGpioValue(int num) {
	GetFlagOnAddress(OFFSET_GPLEV0 / 4, num);
}

void SetGpioValue(int num, bool val) {
	if (val == 0) SetFlagOnOffset(OFFSET_GPCLR0 / 4, num, true);
	else SetFlagOnOffset(OFFSET_GPSET0 / 4, num, true);
}

void SetEventDetectStatus(int num, bool val) {
	SetFlagOnOffset((num > 31 ? OFFSET_GPEDS1 : OFFSET_GPEDS0) / 4, (num > 31 ? num - 31 : num), val);
}

void SetRisingEdgeEnable(int num, bool val) {
	SetFlagOnOffset((num > 31 ? OFFSET_GPREN1 : OFFSET_GPREN0) / 4, (num > 31 ? num - 31 : num), val);
}

void SetHighDetectEnable(int num, bool val) {
	SetFlagOnOffset((num > 31 ? OFFSET_GPHEN1 : OFFSET_GPHEN0) / 4, (num > 31 ? num - 31 : num), val);
}

void SetLowDetectEnable(int num, bool val) {
	SetFlagOnOffset((num > 31 ? OFFSET_GPLEN1 : OFFSET_GPLEN0) / 4, (num > 31 ? num - 31 : num), val);
}

void SetAsyncRisingEdgeDetectEnable(int num, bool val) {
	SetFlagOnOffset((num > 31 ? OFFSET_GPAREN1 : OFFSET_GPAREN0) / 4, (num > 31 ? num - 31 : num), val);
}

void SetAsyncFallingEdgeDetectEnable(int num, bool val) {
	SetFlagOnOffset((num > 31 ? OFFSET_GPAFEN1 : OFFSET_GPAFEN0) / 4, (num > 31 ? num - 31 : num), val);
}

void SetGpioPull(int p) {
	if (p == 0)
		SetFlagOnOffset(OFFSET_GPPUD / 4, 0, 0, 2);
	else if (p > 0)
		SetFlagOnOffset(OFFSET_GPPUD / 4, 0, 2, 2);
	else
		SetFlagOnOffset(OFFSET_GPPUD / 4, 0, 1, 2);
}

void SetGpioPullClock(int num, bool clkset) {
	SetFlagOnOffset(OFFSET_GPPUDCLK0 / 4, num, clkset);
}

int GpioInit(void) {
	int fd_mem = open("/dev/mem", O_RDWR | O_SYNC); // /dev/mem 오픈
	if (fd_mem < 0) {
		printf("no access to /dev/mem\r\n");
		return fd_mem;
	}

	void* gpio_map = mmap(NULL, BLOCK_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd_mem, BASEADDR_REG_GPIO); // GPIO 레지스터 매핑
	void* irq_map = mmap(NULL, BLOCK_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd_mem, BASEADDR_REG_IRQ); // IRQ 레지스터 매핑
	close(fd_mem); // /dev/mem 필요없음

	if (gpio_map == MAP_FAILED) {
		printf("gpio register memory mapping error with code %d\r\n", (int)gpio_map);
		return 1;
	}
	if (irq_map == MAP_FAILED) {
		printf("irq register memory mapping error with code %d\r\n", (int)gpio_map);
		return 1;
	}
	gpio_base = (volatile unsigned*)gpio_map;
	irq_base = (volatile unsigned*)irq_map;

	return 0;
}

void Export(int p) {
	int fs = open("/sys/class/gpio/export", O_WRONLY);
	char str[2];
	sprintf(str, "%d", p);
	write(fs, str, 2);
	close(fs);
}

volatile unsigned* GetGpioBase() {
	return gpio_base;
}

void PrintMemCheck(int base_addr, int start, int end) {
	volatile unsigned* base_map = base_addr == BASEADDR_REG_GPIO ? gpio_base :
		base_addr == BASEADDR_REG_IRQ ? irq_base : gpio_base;

	for (int i = start / 4; i < (end / 4); ++i) {
		int val = (*(base_map + i));
		printf("0x%08x - ", (base_map + i));//(BASEADDR_GPIO)+(i * 4));
		if(base_addr == BASEADDR_REG_GPIO)
			switch (i * 4)
			{
			case OFFSET_GPFSEL0:
				printf("GPFSEL0");
				break;
			case OFFSET_GPFSEL1:
				printf("GPFSEL1");
				break;
			case OFFSET_GPFSEL2:
				printf("GPFSEL2");
				break;
			case OFFSET_GPFSEL3:
				printf("GPFSEL3");
				break;
			case OFFSET_GPFSEL4:
				printf("GPFSEL4");
				break;
			case OFFSET_GPFSEL5:
				printf("GPFSEL5");
				break;
			case OFFSET_GPSET0:
				printf("GPSET0");
				break;
			case OFFSET_GPSET1:
				printf("GPSET1");
				break;
			case OFFSET_GPCLR0:
				printf("GPCLR0");
				break;
			case OFFSET_GPCLR1:
				printf("GPCLR1");
				break;
			case OFFSET_GPLEV0:
				printf("GPLEV0");
				break;
			case OFFSET_GPLEV1:
				printf("GPLEV1");
				break;
			case OFFSET_GPEDS0:
				printf("GPEDS0");
				break;
			case OFFSET_GPEDS1:
				printf("GPEDS1");
				break;
			case OFFSET_GPREN0:
				printf("GPREN0");
				break;
			case OFFSET_GPREN1:
				printf("GPREN1");
				break;
			case OFFSET_GPFEN0:
				printf("GPFEN0");
				break;
			case OFFSET_GPFEN1:
				printf("GPFEN1");
				break;
			case OFFSET_GPHEN0:
				printf("GPHEN0");
				break;
			case OFFSET_GPHEN1:
				printf("GPHEN1");
				break;
			case OFFSET_GPLEN0:
				printf("GPLEN0");
				break;
			case OFFSET_GPLEN1:
				printf("GPLEN1");
				break;
			case OFFSET_GPAREN0:
				printf("GPAREN0");
				break;
			case OFFSET_GPAREN1:
				printf("GPAREN1");
				break;
			case OFFSET_GPAFEN0:
				printf("GPAFEN0");
				break;
			case OFFSET_GPAFEN1:
				printf("GPAFEN1");
				break;
			case OFFSET_GPPUD:
				printf("GPPUD");
				break;
			case OFFSET_GPPUDCLK0:
				printf("GPPUDCLK0");
				break;
			case OFFSET_GPPUDCLK1:
				printf("GPPUDCLK1");
				break;
			default:
				printf("RESERVED");
				break;
			}

		printf("\t\t = %08x", (val));
		printf(" (");
		for (int j = 31; j >= 0; --j) {
			printf("%u", ((val >> j) & 1));
			if (j % 4 == 0 && j != 0) printf(" ");
		}
		printf(")");
		printf("\r\n");
	}
}

void wait2(int ms) {
	struct timespec req_time {
		(time_t)ms/1000,
		(long)(ms%1000)*1000000
	};
	struct timespec nulltime;

	nanosleep(&req_time, &nulltime);
}

int WaitSysfs(int pin, int ms) {
	//int fs, x;
	//uint8_t c;
	//struct pollfd polls;
	char fdname[64];
	uint64_t buffer[1];
	//sysfs > /sys/class/gpio 에 있는 파일시스템 컨트롤 통해서 폴링
	//커널모듈 만들고 ISR, IRQ, 인터럽트 레지스터 테이블 만질라니 시간 너무 걸려서 걍 이걸로 퉁침

	sprintf(fdname, "/sys/class/gpio/gpio%d/value", pin);
	int fd = inotify_init1(IN_CLOEXEC | IN_NONBLOCK);
	inotify_add_watch(fd, fdname, IN_MODIFY);
	while ((read(fd, (char *)buffer, sizeof(buffer))) > 0) {
	}
	//polls.fd = open(fdname, O_RDWR);
	//polls.events = POLLPRI | POLLERR;
	//x = poll(&polls, 1, ms);

	/*if (x > 0) {
		lseek(fs, 0, SEEK_SET);
		read(fs, &c, 1);
	}*/

	//return x;
	return GetGpioValue(pin);
}